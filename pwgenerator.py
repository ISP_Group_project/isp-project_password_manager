from tkinter import Entry, Button, Label    #import the widgets
from tkinter import LabelFrame, Tk, Frame
from tkinter import ttk
import string
from secrets import choice                #The secrets module is used for generating cryptographically strong random numbers
from tkinter.constants import END

# declare the data for generating a random password
UPPERCASE = list(string.ascii_uppercase)
LOWERCASE = list(string.ascii_lowercase)
NUMBER = list(string.digits)
SYMBOLS = ['@', '#', '$', '%', '&', '_']


#------------------------------------------ creating PasswordGenerator class------------------------------#
class PasswordGenerator:

    def __init__(self):
        self.window = Tk()
        self.window.title("RANDOM PASSWORD GENERATOR!")
        self.window.geometry("750x500")
        self.window.iconbitmap("data/img/favicon.ico")
        self.window.configure(bg='dark grey')

        #The comments for the different commands are given below

        #This is the Label Frame
        self.label_frame = LabelFrame(
            self.window, text="Please enter the number of characters to generate the password")
        self.label_frame.pack(pady=50)

        # Below code shows the entry box for number of characters
        self.length_entry_box = ttk.Entry(self.label_frame, width=25)
        self.length_entry_box.pack(padx=45, pady=45)

        # If no length is found then quickly declare the FEEDBACK
        self.feedback = ttk.Label(self.window)

        # This is the code for Entry box for the password
        self.password_entry_box = ttk.Entry(
            self.window, text="", width=55)
        self.password_entry_box.pack(pady=35)

        # Frame for buttons
        self.button_frame = Frame(self.window , bg="dark grey")
        self.button_frame.pack(pady=20)

        # Creating the "Click here to Generate a Password" button
        generate_btn = Button(
            self.button_frame, text="Click here to Generate a Password",  bg="light blue", fg="blue" ,command=self.generate_random_password)
        generate_btn.grid(row=0, column=0, padx=10)

        # Creating the "Copy the Password" Button
        copy_btn = Button(self.button_frame,
                          text="Copy the Password", bg="light blue", fg="blue" , command=self.copy_password)
        copy_btn.grid(row=1, column=0, padx=10)

    def generate_random_password(self):
        self.password_entry_box.delete(0, END)     #delete whatever already present in the password entry box
        try:
            password_length = int(self.length_entry_box.get())   #fetching the value of password length
            
            self.feedback.destroy()            # Destroy the feedback immediately if the length is correct
            data = UPPERCASE + LOWERCASE + NUMBER + SYMBOLS
            password = ''.join(choice(data) for _ in range(password_length)) #choose a random element from data and concatinate the strings
                                                                             #declaring and directly using the seperator''
                                                                         
            self.password_entry_box.insert(0, password)
        except ValueError:             #If error occurs,catch it in the except block and replace the feedback with the error message.
            self.feedback = Label(self.window, fg="blue",
                                  text="Enter the number of characters first !" , bg="dark grey")
            self.feedback.place(x=90, y=110)

    def copy_password(self):
        self.window.clipboard_clear()     #clear the clipboard so that if anything is already copied, it is erased.
        self.window.clipboard_append(self.password_entry_box.get())    #append the value which is located in password_entry_box


if __name__ == "__main__":
    PasswordGenerator().window.mainloop()