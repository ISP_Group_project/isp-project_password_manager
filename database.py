import sqlite3  #do not need a seperate DB server


def init_database():
    with sqlite3.connect("password_vault.db") as db:    #if there is no database then automatically create a one
        cursor = db.cursor()      # return the object of cursor class
    cursor.execute("""
            CREATE TABLE IF NOT EXISTS master(
            id INTEGER PRIMARY KEY,
            password TEXT NOT NULL);
            """)

    cursor.execute("""
            CREATE TABLE IF NOT EXISTS vault(
            id INTEGER PRIMARY KEY,
            platform TEXT NOT NULL,
            userid TEXT NOT NULL,
            password TEXT NOT NULL);
            """)
    return db, cursor   # after executing the commands, return db and cursor to be utilize by other methods and classes.